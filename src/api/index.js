import request from '../utils/request';

export const fetchData = query => {
    return request({
        url: './table.json',
        method: 'get',
        params: query
    });
};

export const fetchBaseField = query => {
    return request({
        url: './basefield.json',
        method: 'get',
        params: query
    });
};


export const fetchRuleData = query => {
    return request({
        url: './rule.json',
        method: 'get',
        params: query
    });
};